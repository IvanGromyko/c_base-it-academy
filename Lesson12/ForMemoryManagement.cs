﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson12
{
    class ForMemoryManagement : IDisposable
    {
        private bool disposed = false;
        public int Number { get; set; }
        ~ForMemoryManagement()
        {
            Dispose(false);
            //finalise
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if(!disposed)
            {
                if(disposing)
                {
                    Console.WriteLine("Управляемые объекты очищены");
                }
                Console.WriteLine("Неуправляемые объекты очищены");
                disposed = true;
            }
        }
    }
}
