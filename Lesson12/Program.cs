﻿ using System;

namespace Lesson12
{
    class Program
    {
        static void Main(string[] args)
        {
            ForMemoryManagement test = new ForMemoryManagement();
            test.Dispose();
            var test1 = new ForMemoryManagement();
            test1.Number = 5;
            test1 = null;            
            GC.Collect();
            while (true) { }
        }
    }
}
