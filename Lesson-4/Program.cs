﻿using System;

namespace Lesson_4
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Enter first integer:");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter second  integer:");
            int y = int.Parse(Console.ReadLine());


            if (x >= 0 && y >= 0)
            {
                if (x == y)
                {
                    Console.WriteLine("Значения равны");
                }
                else if (x > y)
                {
                    Console.WriteLine($"Значение x больше y на {x - y}");
                }
                else
                {
                    Console.WriteLine($"Значение y больше x на {y - x}");
                }
            }

            string Sravnenie = (x > y) ? "x больше y" : "x меньше y";
            Console.WriteLine(Sravnenie);
        }

    }
}
