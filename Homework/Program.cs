﻿using System;

namespace Homework
{
    class Program
    {
        static int Input()
        {
            while (true)
            {
                int a;
                if (int.TryParse(Console.ReadLine(), out a) && a >= 0)
                    return a;
                else
                    Console.WriteLine("Error! Try again!");
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your name:");
            string userName1 = Console.ReadLine();

            Console.WriteLine("Enter number:");
            int checkedNumberUse = Program.Input();

            if (checkedNumberUse != 0)
            {
                for (int i = 0; i < checkedNumberUse; i++)
                {
                    Console.WriteLine($"Hello {userName1}!");
                }
            }
            else
            {
                Console.WriteLine($"Entered 0, but Hello {userName1}!");
            }

            Console.ReadKey();
        }
    }
}
