﻿using System;

namespace Homework_3
{
    class Program
    {
        static double Input()
        {
            while (true)
            {
                double a;
                if (double.TryParse(Console.ReadLine(), out a))
                {
                    if (a > 0)
                    {
                        return a;
                    }
                    Console.WriteLine("Error! Negative number or zero entered. Try again!");
                    continue;
                }
                else
                    Console.WriteLine("Error! Invalid characters entered. Try again!");
            }
        }

        static void Main(string[] args)
        {
            int month = 12;
            double monthProcent = 0;
            double paymentsPerYear = 0;
            double priceFinish;
            string[] nameMonth = new string[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

            Console.WriteLine("Enter credit amount:");
            double summ = Program.Input();

            Console.WriteLine("Enter credit percent:");
            double procent = (Program.Input()) / 100;

            priceFinish = summ;
            Console.WriteLine("Monthly Payments:");
            int x = 0;
            while (month != 0)
            {
                monthProcent = priceFinish * procent;
                paymentsPerYear += priceFinish * procent;
                month--;
                Console.WriteLine($"Payment {nameMonth[0 + x]}: {monthProcent} rub");
                x++;
            }

            Console.WriteLine($"Bank account: {priceFinish} rub");
            Console.WriteLine($"The total amount of payments will be:{paymentsPerYear} rub");
            Console.WriteLine($"The total amount: {paymentsPerYear + priceFinish} rub");


        }
    }
}
