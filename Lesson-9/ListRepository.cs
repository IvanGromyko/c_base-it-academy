﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_9
{
   public class ListRepository : IRepository
    {

        List<int> numbers = new List<int>() { };
        public void Create(int x)
        {
            numbers.Add(x);
        }

        public void Delete(int x)
        {
           numbers.RemoveAt(x);
        }

        public void Read()
        {
            foreach (int i in numbers)
            {
                Console.WriteLine(i);
            }
        }

        public void Update(int index, int x)
        {
            numbers.Insert(index, x);
        }

    }
}
