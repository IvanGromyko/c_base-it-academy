﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson_9
{
    public class ArrayRepository : IRepository
    {
        int[] nums = new int[0];
        public void Create(int x)
        {
            List<int> numsList = nums.ToList();
            numsList.Add(x);
            nums = numsList.ToArray();

        }

        public void Delete(int x)
        {
            List<int> numsList = nums.ToList();
            numsList.RemoveAt(x);
            nums = numsList.ToArray();
        }

        public void Read()
        {
            foreach (int i in nums)
            {
                Console.WriteLine(i);
            }
        }

        public void Update(int index, int x)
        {
            nums[index] = x;
        }
    }
}
