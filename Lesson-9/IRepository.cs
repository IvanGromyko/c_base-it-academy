﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_9
{
    public interface IRepository
    {
        public void Create(int x);
        public void Read ();
        public void Update(int index, int x);
        public void Delete(int x);


    }
}
