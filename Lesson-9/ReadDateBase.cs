﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_9
{
    public class ReadDateBase
    {
        IRepository array = new ArrayRepository();

        public void Create(int x)
        {
            array.Create(x);
        }

        public void Delete(int x)
        {
            array.Delete(x);
        }
            

        public void Read()
        {
            array.Read();
        }

        public void Update(int index, int x)
        {
            array.Update(index, x);
        }


    }
}
