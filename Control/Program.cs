﻿using System;

namespace Control
{
    class Program
    {
        static void Main(string[] args)
        {
            //int i;
            //int j;
            //for (i = 1; i <10; i++)
            //{
            //    for(j = 1; j <10; j++)
            //        Console.WriteLine($"{i} * {j} = {i*j}");
            //}


            int[,] numbers = new int[2, 3] { { 4, 4, 4 }, { 5, 5, 5 }, };
            
            for(int i = 0; i < 2; i++)
            {
                for(int b = 0; b < 3; b++)
                {
                    if(i < b)
                        numbers[i, b] = 1;
                }
            }
            
            for (int i = 0; i < 2; i++)
            {
                for (int b = 0; b < 3; b++)
                {
                    Console.Write(numbers[i, b] + " ");
                }
                Console.WriteLine();
            }


        }
    }
}
