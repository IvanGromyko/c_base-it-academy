﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson_7
{
    public enum minMax
    {
        Max,
        Min
    }
    class SecondClass
    {
        //Поле
        public const int defaultLenght = 20;
        public int myLenght = 15;
        public int[] array;
        public int[] Array
        { 
            get
            {
                return array;
            }
            set
            {
                if (array == null && value.Length <= defaultLenght)
                {
                    int[] array = value;
                }
            }
        }

        public int this[int index]
        {
            get
            {
                return array[index];
            }
            set
            {
                array[index] = value;
            }
        }


        public void Add(int value)
        {
            if (array == null)
            {
                array = new int[myLenght];
            }

            var list = array.ToList();

            if (list.Count < defaultLenght)
            {
                list.Add(value);
            }

            array = list.ToArray();
        }
        bool itemFound = false;
        int indexItem;
        public void Search(int value)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == value)
                {
                    itemFound = true;
                    indexItem = i;
                }
                else
                    itemFound = false;
            }
            if (itemFound == true)
            {
                Console.WriteLine($"Указнное значение ({value}) присутствует в массиве под индексом {indexItem})");
            }
            else
            {
                Console.WriteLine("Элемент не найден");
            }
        }
        int maxValue;
        int minValue;
        public void Search(minMax value)
        {
            if (value == minMax.Max)
            {
                maxValue = array.Max<int>();
                Console.WriteLine(maxValue);
            }
            else
            {
                minValue = array.Min<int>();
                Console.WriteLine(minValue);
            }
        }

        public static SecondClass operator ++(SecondClass secondClass)
        {
            var len = secondClass.array.Length;
            var res = new SecondClass();
            res.array = new int[len];

            for(var i =0; i < len; i++)
            {
                res.array[i] = secondClass.array[i] + secondClass.array[i];
            }
            return res;
        }

        public void ShowElements()
        {
            if (array != null)
            {
                foreach (var item in array)
                {
                    Console.Write($"{item}, ");
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Elements not found.");
            }
        }

        ////Конструктор
        //public SecondClass()
        //{

        //}

        ////Финализатор
        //~SecondClass()
        //{
        //    Console.WriteLine("Disposed!");
    }
}

