﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.ModelPayment
{
    class PaymentModelDTO
    {
        public int IdPayment { get; set; }
        public decimal Amount { get; set; }
        public string DatePayment { get; set; }
        public string Currency { get; set; }
    }
}
