﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.ModelPayment
{
     public class PaymentModel
    {
        public int IdPayment { get; set; }
        public decimal Amount { get; set; }
        public DateTime DatePayment { get; set; }
        public string Currency { get; set; }

    }
}
