﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Program.Validation
{
    public class Date
    {
        public DateTime inputDoB()
        {
            while (true)
            {
                DateTime dob;
                if ((DateTime.TryParseExact(Console.ReadLine(), "dd.MM.yyyy", null, DateTimeStyles.None, out dob)) && (dob.Year > 1910 && dob.Year < 2015))
                    return dob;
                else
                    Console.Write("Ошибка! Попробуйте еще раз.");
            }
        }

            public int GetAge(DateTime birthDate)
        {
            var now = DateTime.Today;
            return now.Year - birthDate.Year - 1 +
                ((now.Month > birthDate.Month || now.Month == birthDate.Month && now.Day >= birthDate.Day) ? 1 : 0);
        }
    }
}
