﻿using FluentValidation.Results;
using Program.ModelPayment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Program.Validation
{
    public class Validation
    {
        public int EnterNumb()
        {
            while (true)
            {
                int i;
                if (int.TryParse(Console.ReadLine(), out i) && i > 0)
                    return i;
                else
                    Console.WriteLine("Ошибка! Попробуйте еще раз.");
            }
        }
        public decimal EnterDec()
        {
            while (true)
            {
                decimal i;
                if (decimal.TryParse(Console.ReadLine(), out i))
                    return i;
                else
                    Console.WriteLine("Ошибка! Попробуйте еще раз.");
            }
        }
        public string EnterString()
        {
            while (true)
            {
                string toReturn;
                if (Regex.IsMatch(toReturn = Console.ReadLine(), @"^[a-zA-Zа-яА-Я]{1,20}$"))
                    return toReturn;
                else
                    Console.WriteLine("Ошибка! Попробуйте еще раз.");
            }
        }
        public string EnterAdress()
        {
            while (true)
            {
                string toReturn;
                if ((toReturn = Console.ReadLine()) != null)
                    return toReturn;
                else
                    Console.WriteLine("Ошибка! Попробуйте еще раз.");
            }
        }
    }
}


