﻿using Program.ModelMember;
using Program.ModelPayment;
using Program.DateBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Validation
{
    public class Reader
    {
        Date date = new Date();
        Validation validation = new Validation();
        public void RegMember(MemberModel member)
        {
            

            Console.WriteLine("Введите имя:");
            member.FirstName = validation.EnterString();
            Console.WriteLine("Введите фамилию:");
            member.LastName = validation.EnterString();
            Console.WriteLine("Введите дату рождения в формате дд.мм.гггг (день.месяц.год):");
            member.Dob = date.inputDoB();
            Console.WriteLine("Введите ID пользователя:");
            member.IdMember = validation.EnterNumb();
            Console.WriteLine("Введите адрес 'страна, город, улица, дом':");
            member.Address = validation.EnterAdress();
            member.Age = date.GetAge(member.Dob);

            BaseMembers.members.Add(member);

            Console.WriteLine();
            Console.WriteLine($"Добавлен пользователь:\n" +
            $"ID: {member.IdMember}\n Name: {member.FirstName} {member.LastName}\n Date of birthday:{member.Dob.ToShortDateString()}\n Возраст - {member.Age}\n");

        }

        public void RegPayment(PaymentModel payment)
        {

            Console.WriteLine("Введите ID платежа:");
            payment.IdPayment = validation.EnterNumb();
            Console.WriteLine("Введите сумму платежа:");
            payment.Amount = validation.EnterDec();
            Console.WriteLine("Введите валюту, в которой будет совершаться платеж:");
            payment.Currency = validation.EnterString();
            Console.WriteLine("Введите дату платежа в формате дд.мм.гггг (день.месяц.год):");
            payment.DatePayment = date.inputDoB();
            Console.WriteLine();

            BasePayments.payments.Add(payment);
            Console.WriteLine($"Добавлен платеж:\n " +
            $"ID: {payment.IdPayment}\n Сумма: {payment.Amount}\n Валюта: {payment.Currency}\n Дата:{payment.DatePayment.ToShortDateString()}\n");
        }
    }
}

