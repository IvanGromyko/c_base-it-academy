﻿using Program.DateBase;
using Program.ModelMember;
using Program.ModelPayment;
using Program.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Service
{
    class Interface
    {
        Reader readConsole = new Reader();
        BaseMembers baseMember = new BaseMembers();
        BasePayments basePayments = new BasePayments();

        public void Interf()
        {
            while (true)
            {
                Console.WriteLine("(1) - регистрация нового пользователя\n "+
                "(2) - регистрация нового платежа\n"+
                "(3) - список пользователей\n "+
                "(4) - список платежей\n"+
                "(x) - выход из программы");
                string selection = Console.ReadLine();
                switch (selection)
                {
                    case "1":
                        MemberModel p1 = new MemberModel();
                        readConsole.RegMember(p1);
                        break;
                    case "2":
                        PaymentModel pay1 = new PaymentModel();
                        readConsole.RegPayment(pay1);
                        break;
                    case "3":
                        baseMember.DisplayListMembers();
                        break;
                    case "4":
                        basePayments.DisplayListPayments();
                        break;
                    case "x":
                        return;
                    default:
                        break;

                }
            }

        }
    }

}
