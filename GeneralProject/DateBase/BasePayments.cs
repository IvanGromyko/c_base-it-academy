﻿using Program.ModelPayment;
using System;
using System.Collections.Generic;
using System.Text;

namespace Program.DateBase
{
    public class BasePayments
    {
        public static List<PaymentModel> payments = new List<PaymentModel>();

        public void DisplayListPayments()
        {
            Console.WriteLine("Список платежей:");
            foreach (PaymentModel p in payments)
            {
                Console.WriteLine($"ID:{p.IdPayment} - {p.DatePayment.ToShortDateString()} - {p.Amount}{p.Currency}");
            }
        }
    }
}
