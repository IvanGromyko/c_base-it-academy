﻿using Program.ModelMember;
using System;
using System.Collections.Generic;
using System.Text;

namespace Program.DateBase
{
    public class BaseMembers
    {
        public static List<MemberModel> members = new List<MemberModel>();

        public void DisplayListMembers()
        {
            Console.WriteLine("Список пользователей:");
            foreach (MemberModel p in members)
            {
                Console.WriteLine($"ID:{p.IdMember} - {p.FirstName}{p.LastName} - {p.Dob.ToShortDateString()} ");
                Console.WriteLine();
            }
        }
}
}
