﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.ModelMember
{
    public class MemberModel
    {
        public int IdMember { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public DateTime Dob { get; set; }
        public decimal AccountBalance { get; set; }

        public void DisplayInfo()
        {
            Console.WriteLine($"ID: {IdMember} Name: {FirstName} {LastName} Date of birthday:{Dob}");
        }
    }
}
