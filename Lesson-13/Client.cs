﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_13
{
    class Client
    {
        public string Name { get; set; }

        public Client(string name)
        {
            this.Name = name;
        }

        public void ShowNews(string news)
        {
            Console.WriteLine($"Пользователь {Name} подписался на новтсь ");
        }

    }
}
