﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson_13
{
    public enum NewsType { News, Sport, Humor }
    public class NewsProvider
    {
        public delegate void NewNews(string message);
        public event NewNews ProvideSport;
        public event NewNews ProvideHumor;
        public event NewNews ProvideNews;


        public void SetNews(NewsType newsType, string news)
        {
            switch (newsType)
            {
                case NewsType.Sport:
                    ProvideSport?.Invoke(news);
                    break;
                case NewsType.Humor:
                    ProvideSport?.Invoke(news);
                    break;
                default: 
                    ProvideSport?.Invoke(news);
                    break;
            }
        }
        
        
    }
}
