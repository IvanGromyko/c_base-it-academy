﻿using System;

namespace Homework_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Implicit type conversion
            //Example 1
            int a = 100;
            short b = (short)a;

            //Example 2
            decimal c = 100;
            double d = (double)c;

            //Example 3
            long e = 128;
            byte f = (byte)e;
            Console.WriteLine(f);

            //Explicit type conversion
            //Example 1
            long g = 100;
            double h = g;

            //Example 2
            ushort k = 100;
            int l = k;

            //Example 3
            float m = 100;
            double n = m;

            //Boxing operation
            int x = 100;
            Console.WriteLine(x.GetType());
            object y = x;

            //Unboxing operation
            Console.WriteLine(y.GetType());
            y = 99;
            int UnboxInt = (int)y;
            Console.WriteLine(UnboxInt);
        }
    }
}
