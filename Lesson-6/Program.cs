﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Lesson_6
{
    class Program
    {
        public static void ShowColl(ModelCollection list)
        {
            String xx = new String('_', 50);
            foreach (var myArray2 in list)
            {
                Console.WriteLine(myArray2.Name.ToString());
            }
            Console.WriteLine(xx);
        }
        static void Main(string[] args)
        {
            ModelCollection peoples = new ModelCollection();
            Model people1 = new Model();

            people1.Name = "Ivan";
            people1.Age = -1;

            peoples.Add(people1);
            ShowColl(peoples);


            Model people2 = new Model();
            people2.Name = "John";
            people2.Age = 11;
            ShowColl(peoples);



        }

    }
}

public enum ModelType
{
    Old,
    Young
}


//public static void ShowColl(IEnumerable list)
//{
//    String xx = new String('_', 50);
//    foreach (var myArray in list)
//    {
//        Console.WriteLine(myArray.ToString());
//    }
//    Console.WriteLine(xx);
//}

//static void Main(string[] args)
//{
//    ArrayList array = new ArrayList() { 9, 77, 100, 1000 };
//    List<int> listColl = new List<int>() { 5, 33, 56, 3 };

//    ShowColl(array);
//    ShowColl(listColl);

//    array.Add(15);
//    array.Add(7);

//    listColl.Add(7);
//    listColl.Add(9);

//    ShowColl(array);
//    ShowColl(listColl);

//    array.RemoveAt(3);
//    listColl.RemoveAt(3);

//    ShowColl(array);
//    ShowColl(listColl);

//    array.Remove(7);
//    listColl.Remove(9);

//    ShowColl(array);
//    ShowColl(listColl);


//    array.Sort();
//    listColl.Sort();

//    ShowColl(array);
//    ShowColl(listColl);







