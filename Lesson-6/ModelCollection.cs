﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Lesson_6
{
    public class ModelCollection: Collection<Model>
    {
        protected override void InsertItem(int index, Model item)
        {
            if (String.IsNullOrEmpty(item.Name)) Console.WriteLine("Error! Name is empty or null.");
            else 
            {
                base.InsertItem(index, item);
                Console.WriteLine("Insert");
                
            }
            if (item.Age < 0) Console.WriteLine("Error! Age <0");
            else 
            {
                base.InsertItem(index, item);
                Console.WriteLine("Insert");
            }
        }

        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            Console.WriteLine("Remove");
        }
    }
}
